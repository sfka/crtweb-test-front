document.addEventListener('DOMContentLoaded', function () {
    hideHeader();
    getSplice();
    getLongSequence();
    switchBlocks();
});

function hideHeader() {
    let hideBtn = document.querySelector('.header-turn');
    let header = document.querySelector('.header')
    hideBtn.onclick = function () {
        if (header.classList.contains('header_hidden')) {
            header.classList.remove('header_hidden');
        } else {
            header.classList.add('header_hidden');
        }
    }
}

function getSplice() {
    let ar = [1, 2, 3, 4, 5];
    let step = 3;
    ar = ar.concat(ar.splice(0, ar.length - step));

    console.log(ar)
}

function getLongSequence() {
    let a = 'aababba';
    let b = 'abbaabcd';
    let longest = 0;
    let longestStr = '';

    for (let i = 0; i < a.length; i++) {
        for (let j = i + 1; j <= a.length; j++) {
            let newB = a.substring(i, j);
            let pos = b.lastIndexOf(newB);
            if (pos >= 0 && newB.length > longest) {
                longest = newB.length;
                longestStr = newB;
            }
        }
    }
    console.log(longestStr);
}

function switchBlocks() {
    let switcher = document.querySelectorAll('.js__checkbox-switch');
    for (let i = 0; i < switcher.length; i++) {
        switcher[i].onchange = function (event) {
            let blockAttr = event.target.value;
            let checked = event.target.checked;
            let blocks = document.querySelectorAll('div[data-block="' + blockAttr + '"]');
            if (checked) {
                for (let j = 0; j < blocks.length; j++) {
                    blocks[j].classList.add('hidden');
                }
            } else {
                for (let j = 0; j < blocks.length; j++) {
                    blocks[j].classList.remove('hidden');
                }
            }
        }
    }

}