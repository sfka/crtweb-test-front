<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css"
          href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/main.css">
    <title>Document</title>
</head>
<? $arBlocks = [
    ['name' => 'Блок 1', 'checkbox' => 1],
    ['name' => 'Блок 1', 'checkbox' => 1],
    ['name' => 'Блок 3', 'checkbox' => 3],
    ['name' => 'Блок 4', 'checkbox' => 4],
];
$arInputs = array_column($arBlocks, null, 'checkbox'); ?>
<body class="page">
<header class="header">
    <div class="header__top">
        <div class="header-turn js__header-turn">
            <span class="header-turn__line"></span>
        </div>
    </div>
    <div class="header__body">
        <nav class="menu">
            <div class="menu__item selected">
                <a href="" class="menu__item-link">
                    <i class="fa fa-home"></i>
                    <span>Главная</span>
                </a>
            </div>
            <div class="menu__item">
                <a href="" class="menu__item-link">
                    <i class="fa fa-building"></i>
                    <span>О компании</span>
                </a>
            </div>
            <div class="menu__item">
                <a href="" class="menu__item-link">
                    <i class="fa fa-map-marker"></i>
                    <span>Контакты</span>
                </a>
            </div>
        </nav>
    </div>
</header>
<div class="content">
    <div class="switcher">
        <div class="input-switcher">
            <? foreach ($arInputs as $arBlock): ?>
                <label>
                    <input type="checkbox" value="<?= $arBlock['checkbox'] ?>" name="<?= $arBlock['checkbox'] ?>"
                           class="js__checkbox-switch">
                    <span><?= $arBlock['name'] ?></span>
                </label>
            <? endforeach; ?>
        </div>
        <div class="block-switcher">
            <? foreach ($arBlocks as $arBlock): ?>
                <div class="block-switcher__item" data-block="<?= $arBlock['checkbox'] ?>"><?= $arBlock['name'] ?></div>
            <? endforeach; ?>
        </div>
    </div>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci alias architecto aspernatur, cupiditate
        deleniti, expedita harum in iusto molestiae mollitia nisi nulla quaerat quasi reiciendis rerum? Consequuntur
        quasi velit voluptatem.</p>
</div>
<script src="js/main.js"></script>
</body>
</html>